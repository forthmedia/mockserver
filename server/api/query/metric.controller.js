'use strict';

var _ = require('lodash');
var logins = require('./logins.model');
var q = require('q');

// Get logins aggregation
exports.logins = function (req, res) {

    var metrics = {};
    var promisA = logins.aggregate([
        {"$unwind": "$logins"},
        {$group: {_id: "$organisation", cnt: {$sum: "$logins.value"}}}
    ]).exec();
    var promisB = logins.aggregate([
        {"$unwind": "$logins"},
        {$group: {_id: "$organisation", avg: {$avg: "$logins.value"}}}
    ]).exec();
    var promisC = logins.aggregate([
        {"$unwind": "$logins"},
        {$group: {_id: { $week : "$logins.date" } , cnt: {$sum: "$logins.value"}}}
    ]).exec();
    var promisD = logins.aggregate([
        {"$unwind": "$logins"},
        {$group: {_id: { $dayOfWeek : "$logins.date" } , cnt: {$sum: "$logins.value"}}}
    ]).exec();
    var promisE = logins.aggregate([
        {"$unwind": "$logins"},
        {$group: {_id: { $month : "$logins.date" } , cnt: {$sum: "$logins.value"}}}
    ]).exec();
    q.all([promisA, promisB , promisC , promisD , promisE ]).then(function (test) {
        metrics.sum = test[0];
        metrics.avg = test[1];
        metrics.week = test[2];
        metrics.dayOfWeek = test[3];
        metrics.month = test[4];
        return res.status(200).send(metrics);
    })

};


function handleError(res, err) {
    return res.status(500).send(err);
}