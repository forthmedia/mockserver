'use strict';

var express = require('express');
var controller = require('./metric.controller');

var router = express.Router();

router.get('/logins', controller.logins);

module.exports = router;