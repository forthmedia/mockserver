'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Login = new Schema({
    date: Date,
    value: Number
});

var Logins = new Schema({
    organisation: String,
    country: String,
    city: String,
    name: String,
    logins: [ Login ]
});

module.exports = mongoose.model('logins', Login);