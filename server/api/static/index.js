'use strict';

var express = require('express');

var router = express.Router();

router.get('/:file', function ( req , res ) {

    var test = require('../../../JSON/DATA/' + req.params.file);

    return res.status(200).send(test);

});

module.exports = router;