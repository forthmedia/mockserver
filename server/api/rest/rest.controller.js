///**
// * Using Rails-like standard naming convention for endpoints.
// * GET     /users              ->  index
// * POST    /users              ->  create
// * GET     /users/:id          ->  show
// * PUT     /users/:id          ->  update
// * DELETE  /users/:id          ->  destroy
// */
//
'use strict';

var _ = require('lodash');

// Get list of users
exports.index = function(req, res) {
  var obj = require('./' + req.params.obj + '.model');
  obj.find(function (err, objects) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(objects);
  });
};

// Get a single user
exports.show = function(req, res) {
  var obj = require('./' + req.params.obj + '.model');
  obj.findById(req.params.id, function (err, object) {
    if(err) { return handleError(res, err); }
    if(!object) { return res.status(404).send('Not Found'); }
    return res.json(object);
  });
};

// Creates a new user in the DB.
exports.create = function(req, res) {
  var obj = require('./' + req.params.obj + '.model');
  obj.create(req.body, function(err, object) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(object);
  });
};

// Updates an existing user in the DB.
exports.update = function(req, res) {
  var obj = require('./' + req.params.obj + '.model');
  if(req.body._id) { delete req.body._id; }
  obj.findById(req.params.id, function (err, object) {
    if (err) { return handleError(res, err); }
    if(!object) { return res.status(404).send('Not Found'); }
    var updated = _.merge(object, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(object);
    });
  });
};

// Deletes a user from the DB.
exports.destroy = function(req, res) {
  var obj = require('./' + req.params.obj + '.model');
  obj.findById(req.params.id, function (err, object) {
    if(err) { return handleError(res, err); }
    if(!object) { return res.status(404).send('Not Found'); }
    user.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}