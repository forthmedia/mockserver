'use strict';

var express = require('express');
var controller = require('./rest.controller');

var router = express.Router();

router.get('/:obj', controller.index);
router.get('/:obj/:id', controller.show);
router.post('/:obj/', controller.create);
router.put('/:obj/:id', controller.update);
router.patch('/:obj/:id', controller.update);
router.delete('/:obj/:id', controller.destroy);

module.exports = router;