'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var userSchema = new Schema({

    name: String,
    preferences: Object

});

module.exports = mongoose.model('user', userSchema);