'use strict';

var _ = require('lodash');
var utils = require('./utils');

exports.timeseries = function (req, res) {

    var result = utils.stream_layers(3, 365, 1 , "time").map(function (data, i) {
        return {
            key: 'Stream #' + i,
            values: data
        };
    });

    return res.status(200).send(result);

};

exports.series = function (req, res) {

    var result = utils.stream_layers(3, 365, 1 , "index").map(function (data, i) {
        return {
            key: 'Stream #' + i,
            values: data
        };
    });

    return res.status(200).send(result);

};

function handleError(res, err) {
    return res.status(500).send(err);
}