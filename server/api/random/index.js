'use strict';

var express = require('express');
var controller = require('./random.controller');

var router = express.Router();

router.get('/timeseries', controller.timeseries);
router.get('/series', controller.series);

module.exports = router;