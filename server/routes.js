/**
 * Main application routes
 */

'use strict';

var path = require('path');

module.exports = function(app) {

  // Insert routes below
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
  });
  app.use('/api/random', require('./api/random'));
  app.use('/api/rest', require('./api/rest'));
  app.use('/api/static', require('./api/static'));
  app.use('/api/query', require('./api/query'));
  app.get('/', function (req, res) {
    res.render('index')
  });
  app.use(function (err, req, res, next) {
    console.error(err.message);
    next();
//    res.status(500).send('Something broke!')

  });

  app.get('*', function(req, res){
    res.status(404).send('what??? - couldn\'t get that - check the console');
  });
};
